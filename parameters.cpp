#include "parameters.h"

Parameters::Parameters()
{
    askRedraw=true;
    paused=false;
    ticks=ticksPerSec=0;
    ticksLimit=15;
}

Parameters* Parameters::getInstance()
{
    static Parameters *instance=nullptr;
    if(instance==nullptr){
        instance = new Parameters;
    }

    return instance;
}

bool Parameters::getShowCountry() const
{
    return showCountry;
}

void Parameters::setShowCountry(bool value)
{
    showCountry = value;
    askRedraw=true;
}

bool Parameters::getShowElevation() const
{
    return showElevation;
}

void Parameters::setShowElevation(bool value)
{
    showElevation = value;
    askRedraw=true;
}

bool Parameters::getShowPopulation() const
{
    return showPopulation;
}

void Parameters::setShowPopulation(bool value)
{
    showPopulation = value;
    askRedraw=true;
}

bool Parameters::getAskRedraw() const
{
    return askRedraw;
}

void Parameters::setAskRedraw(bool value)
{
    askRedraw = value;
}

bool Parameters::getPaused() const
{
    return paused;
}

void Parameters::setPaused(bool value)
{
    paused = value;
}

int Parameters::getTicks() const
{
   // QMutexLocker locker(&mutex);
    return ticks;
}

void Parameters::incrementTicks()
{
    //QMutexLocker locker(&mutex);
    ticks++;
    ticksPerSec++;
}

int Parameters::getTicksPerSec() const
{
    return ticksPerSec;
}

void Parameters::setTicksPerSec(int value)
{
    ticksPerSec = value;
}

int Parameters::getTicksLimit() const
{
    return ticksLimit;
}

void Parameters::setTicksLimit(int value)
{
    ticksLimit = value;
}

bool Parameters::getShowTemperature() const
{
    return showTemperature;
}

void Parameters::setShowTemperature(bool value)
{
    showTemperature = value;
}

StatsWindow *Parameters::getStats() const
{
    return stats;
}

void Parameters::setStats(StatsWindow *value)
{
    stats = value;
}
