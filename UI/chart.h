#ifndef CHART_H
#define CHART_H

#include <QChart>
#include <QDateTimeAxis>
#include <QLineSeries>
#include <QValueAxis>



class Chart
{
public:
    Chart(const QString &name, const QString &dataName);

    QtCharts::QChart *getChart() const;

    QtCharts::QLineSeries *getSeries() const;

    void append(const QDateTime &date, qreal value);
    void update();

private:
    QtCharts::QChart *chart;
    QtCharts::QValueAxis *axisY;
    QtCharts::QDateTimeAxis *axisX;
    QtCharts::QLineSeries *series;

    qreal maxValue;
    qreal minValue;

    void appendToChartView(const QDateTime &date, qreal value);

    QVector<QDateTime> tempDates;
    QVector<qreal> tempValues;



};

#endif // CHART_H
