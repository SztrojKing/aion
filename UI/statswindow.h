#ifndef STATSWINDOW_H
#define STATSWINDOW_H

#include <QChart>
#include <QChartView>
#include <QDateTimeAxis>
#include <QLineSeries>
#include <QMainWindow>
#include <QTimer>
#include <QValueAxis>
#include <chart.h>

namespace Ui {
class StatsWindow;
}

class StatsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StatsWindow(QWidget *parent = nullptr);
    ~StatsWindow();

    void addAchart(const QString &name, Chart *chart);

    Chart* getChart(const QString &name);

private:
    Ui::StatsWindow *ui;
    QTimer *chartsTimer;
    QVector<QtCharts::QChartView*> chartViews;
    QMap<QString, Chart*> charts;

public slots:
    void updateCharts();


};

#endif // STATSWINDOW_H
