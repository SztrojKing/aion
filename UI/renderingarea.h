#ifndef RENDERINGAREA_H
#define RENDERINGAREA_H
#include <QOpenGLWidget>
#include "worldmap.h"
#include <QVector>

class RenderingArea : public QOpenGLWidget
{
    Q_OBJECT
public:
    RenderingArea(QWidget *parent);
    virtual ~RenderingArea();

    void setMap(WorldMap* map);
    // QOpenGLWidget interface
protected:
    void initializeGL();

    void resizeGL(int w, int h);
    void paintGL();

    void updateScale();

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    QTimer *screenTimer;
    QTimer *refreshTimer;
    QPainter *painter;

    WorldMap* map;
    double scale;
    double minScale;
    double maxScale;
    int mousePressX;
    int mousePressY;
    bool mousePressed;
    int mapX;
    int mapY;
    int mapWidth;
    int mapHeight;
    QVector<QColor> countriesColor;

    QColor mixColors(const QColor &a, const QColor &b);

    QImage *renderingImage;

public slots:
    void askRedraw();

};

#endif // RENDERINGAREA_H
