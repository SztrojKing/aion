#include "chart.h"

#include <QDateTime>

using namespace QtCharts;

Chart::Chart(const QString &name, const QString &dataName)
{
    minValue=0;
    maxValue=1;

    series = new QLineSeries();
    chart = new QChart();
    axisX = new QDateTimeAxis();
    axisY = new QValueAxis();


    //chart->legend()->hide();
    chart->setTitle(name);


    axisX->setFormat("dd/MM/yyyy");
    axisX->setTitleText("Date");
    axisX->setRange(QDateTime(QDate(2020, 1, 1)), QDateTime(QDate(2020, 1, 2)));

    chart->addAxis(axisX, Qt::AlignBottom);


    axisY->setLabelFormat("%i");
    axisY->setTitleText(dataName);
    axisY->setRange(0, 1);
    chart->addAxis(axisY, Qt::AlignLeft);

    chart->addSeries(series);

    series->attachAxis(axisY);
    series->attachAxis(axisX);

    //series->append(QDateTime(QDate(2020, 1, 1)).toMSecsSinceEpoch(), 2000);
    // series->append(QDateTime(QDate::currentDate().addDays(2)).toMSecsSinceEpoch(), 10000000);


    //series->append(QDateTime().toSecsSinceEpoch(), 100000);
    //series->append(10000, 10000000);

}

QtCharts::QChart *Chart::getChart() const
{
    return chart;
}

QtCharts::QLineSeries *Chart::getSeries() const
{
    return series;
}

void Chart::append(const QDateTime &date, qreal value)
{
    tempDates.push_back(date);
    tempValues.push_back(value);
}

void Chart::appendToChartView(const QDateTime &date, qreal value)
{
    if(value > maxValue || maxValue==1.0){
        maxValue = value;
        axisY->setMax(value*1.1);
    }
    if(value < minValue  || minValue==0.0){
        minValue=value;
        axisY->setMin(value < 0.0 ? value*1.1 : value*0.9);

    }
    axisX->setMax(date);
    series->append(date.toMSecsSinceEpoch(), value);


}

void Chart::update()
{
    if(tempDates.size()>0){
        chart->removeSeries(series);
        chart->removeAllSeries();
        for(int i=0;i<tempDates.size();i++){
            appendToChartView(tempDates[i], tempValues[i]);
        }

        tempDates.clear();
        tempValues.clear();

        chart->addSeries(series);
    }

}
