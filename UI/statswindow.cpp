#include "statswindow.h"
#include "ui_statswindow.h"

#include <QChartView>
#include <QThread>


StatsWindow::StatsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StatsWindow)
{
    ui->setupUi(this);
    chartsTimer = new QTimer(0);
    QThread *timerThread = new QThread();
    chartsTimer->moveToThread(timerThread);
    chartsTimer->setInterval(5000);
    chartsTimer->setSingleShot(false);

    connect(chartsTimer, SIGNAL(timeout()), this, SLOT(updateCharts()), Qt::QueuedConnection);
    connect(timerThread, SIGNAL(started()),chartsTimer,  SLOT(start()));
    timerThread->start();

}

StatsWindow::~StatsWindow()
{
    delete ui;
}

void StatsWindow::addAchart(const QString &name, Chart *chart)
{

    QtCharts::QChartView *chartView = new QtCharts::QChartView(this);
    ui->chartsContainer->addWidget(chartView, charts.size()/2, charts.size()%2);
    chartView->setChart(chart->getChart());

    charts.insert(name, chart);

}

Chart *StatsWindow::getChart(const QString &name)
{
    return charts[name];
}

void StatsWindow::updateCharts()
{
    if(this->isVisible()){
        for(auto e : charts.values())
        {
            e->update();
        }
    }
}




