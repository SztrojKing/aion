#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "statswindow.h"
#include <Multithreading/multithreadedworker.h>
#include "worldmap.h"
#include "parameters.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_showCountry_toggled(bool checked);

    void on_showElevation_toggled(bool checked);

    void on_showPopulation_toggled(bool checked);

    void on_showTemperature_toggled(bool checked);

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    WorldMap *map;
    MultithreadedWorker *worker;
    Parameters *p;
    StatsWindow *statsWindow;
};
#endif // MAINWINDOW_H
