#include "renderingarea.h"
#include <QPainter>
#include <QTimer>
#include <QVector>
#include "parameters.h"
#include "tile.h"
#include "value.h"
#include <QtGlobal>
#include <QtMath>
#include <QRandomGenerator>
#include <QWheelEvent>
#include <QTime>

RenderingArea::RenderingArea(QWidget *parent): QOpenGLWidget(parent)
{
    renderingImage = new QImage;
    painter = new QPainter;


    screenTimer = new QTimer;
    connect(screenTimer,SIGNAL(timeout()),this,SLOT(update()));

    map=nullptr;

    refreshTimer = new QTimer;
    connect(refreshTimer,SIGNAL(timeout()),this,SLOT(askRedraw()));


    // compute random colors for each country id
    QRandomGenerator random(10);
    for(int i=0;i<400;i++){
        int r = (random.generate()%(255-100)) + 50;
        countriesColor.push_back(QColor::fromRgb(r, r, r));
    }
    scale = 1;
    mapX=mapY=0;
    maxScale=10.0;

    screenTimer->setSingleShot(true);
    screenTimer->start(1000);

    refreshTimer->start(500);

}

RenderingArea::~RenderingArea(){
    delete painter;
    delete screenTimer;
    delete renderingImage;
    delete refreshTimer;
}
void RenderingArea::setMap(WorldMap *map)
{
    this->map = map;
    QVector<QVector<Tile>> &tiles = map->getTiles();
    mapWidth = tiles.size();
    mapHeight = tiles[0].size();

}

void RenderingArea::initializeGL()
{
    QOpenGLWidget::initializeGL();
}

void RenderingArea::resizeGL(int w, int h)
{
    QOpenGLWidget::resizeGL(w, h);
    updateScale();
    delete renderingImage;
    renderingImage = new QImage(width(), height(), QImage::Format_RGB888);
}

void RenderingArea::paintGL()
{


    Parameters *p = Parameters::getInstance();

    QTime currentFrameDuration;
    currentFrameDuration.start();

    painter->begin(this);
    painter->setRenderHint(QPainter::Antialiasing);

    // redraw buffer ?
    if(p->getAskRedraw() && map!=nullptr ){

        QPainter imagePainter(renderingImage);

        QVector<QVector<Tile>> &tiles = map->getTiles();

        // draw onscreen tiles
        for(int x=qFloor(mapX/scale); x*scale - mapX<width() && x<mapWidth;x++){
            for(int y=qFloor(mapY/scale); y*scale - mapY<height() && y<mapHeight;y++){
                Tile &tile = tiles[x][y];
                QColor color(Qt::white);
                // scaling
                double scaledX = x*scale - mapX;
                double scaledY = y*scale - mapY;
                int rectWidth = qCeil(scaledX+scale) - qCeil(scaledX);
                int rectHeight = qCeil(scaledY+scale) - qCeil(scaledY);
                int rectX = qCeil(scaledX);
                int rectY = qCeil(scaledY);

                // draw countries
                Value &country = tile.getCountry();
                if(p->getShowCountry()){
                    QColor countryColor = country.isValid() ? countriesColor[(int)country.getValue()]
                            : QColor::fromRgb(0, 105, 148);

                    color = mixColors(countryColor, color);
                }


                // draw elevation
                Value &elevation = tile.getElevation();
                if(elevation.isValid() && p->getShowElevation()){
                    double altitude = elevation.getValue();
                    QColor elevationColor;
                    if(altitude>=0){
                        double altitudeRatio = altitude/8000.0;
                        if(altitude<=50){
                            altitudeRatio = altitude/50;
                            elevationColor.setRgb(255-altitudeRatio*255, 255, 0, 128);
                        }
                        else{
                            elevationColor.setRgb(altitudeRatio*155, 255-altitudeRatio*200, altitudeRatio*42, 128);
                        }

                    }
                    else{
                        double altitudeRatio = -altitude/12000.0;
                        elevationColor.setRgb(255-altitudeRatio*255, 255-altitudeRatio*255, 255);
                    }
                    color = mixColors(elevationColor, color);

                }

                // draw temperature
                Value &temperatureValue = tile.getTemperature();
                if(temperatureValue.isValid() && p->getShowTemperature()){
                    QColor temperatureColor;
                    double temperature = temperatureValue.getValue();
                    if(temperature<=0){
                        double temperatureRatio = qMin(-temperature/50.0, 1.0);
                        temperatureColor.setRgb(255-temperatureRatio*255, 255-temperatureRatio*255, 255, 128);
                    }
                    else{
                        double temperatureRatio = qMin(temperature/45.0, 1.0);
                        temperatureColor.setRgb(255, 255-temperatureRatio*255, 0, 128);
                    }

                    color = mixColors(temperatureColor, color);
                }

                // draw population
                Value &population = tile.getPopulation();
                if(population.isValid() && p->getShowPopulation()){
                    double populationRatio = (qMax(0.0, qLn(population.getValue()/5000)))/(qLn(population.getMaxValue()/5000));
                    color = mixColors(QColor::fromRgb(255, 0, 255-populationRatio*255, qFloor(populationRatio*255.0)), color);
                }

                // draw tile with computed color
                imagePainter.fillRect(rectX, rectY, rectWidth, rectHeight, color);
            }
        }



        imagePainter.drawText(10, 16, map->getDate().toString("dd/MM/yyyy"));


        p->setAskRedraw(false);
    }

    painter->drawImage(0,0, *this->renderingImage);
    painter->end();

    screenTimer->start(qMax(1000/30 - currentFrameDuration.elapsed(), 5));


}

void RenderingArea::updateScale()
{
    minScale = qMax( double(this->width())/mapWidth, (double(this->height())/mapHeight));
    scale = qMin(qMax(scale, minScale), maxScale);

    mapX = qMax(mapX, 0);
    if(mapWidth*scale - mapX<this->width()){
        mapX=mapWidth*scale - this->width();
    }
    mapY = qMax(mapY, 0);
    if(mapHeight*scale - mapY<this->height()){
        mapY=mapHeight*scale - this->height();
    }


    Parameters::getInstance()->setAskRedraw(true);
}

void RenderingArea::mousePressEvent(QMouseEvent *event)
{
    mousePressX = event->x()/scale;
    mousePressY = event->y()/scale;


    mousePressed=true;

    // try to find bot
    /*
    int realMouseX = mousePressX + p->getX()/p->getScale();
    int realMouseY = mousePressY + p->getY()/p->getScale();
    */
}



void RenderingArea::mouseReleaseEvent(QMouseEvent *event)
{
    mousePressed=false;
}

void RenderingArea::mouseMoveEvent(QMouseEvent *event)
{
    if(mousePressed){
        int mouseReleaseX = event->x()/scale;
        int mouseReleaseY = event->y()/scale;

        int dx = mouseReleaseX - mousePressX;
        int dy = mouseReleaseY - mousePressY;

        mapX = mapX - dx*scale;

        mapY = mapY - dy*scale;

        mousePressEvent(event);
        updateScale();
    }
}


void RenderingArea::wheelEvent(QWheelEvent *event)
{
    double zoom = event->delta()/500.0*1;
    scale += zoom;
    if(scale<=maxScale){

        mapX += (zoom > 0 ? 1 : - 1)*event->x()/scale;
        mapY += (zoom > 0 ? 1 : - 1)*event->y()/scale;
    }
    updateScale();
}

QColor RenderingArea::mixColors(const QColor &a, const QColor &b)
{
    QColor r;

    double outA = a.alphaF() + b.alphaF()*(1-a.alphaF());

    r.setRedF((a.redF()*a.alphaF()+ b.redF()*b.alphaF()*(1-a.alphaF()))/outA);
    r.setGreenF((a.greenF()*a.alphaF()+ b.greenF()*b.alphaF()*(1-a.alphaF()))/outA);
    r.setBlueF((a.blueF()*a.alphaF()+ b.blueF()*b.alphaF()*(1-a.alphaF()))/outA);


    return r;
}

void RenderingArea::askRedraw()
{
    Parameters *p = Parameters::getInstance();
    p->setAskRedraw(true);
}

