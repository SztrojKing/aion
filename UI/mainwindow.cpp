#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <gdal.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    statsWindow = new StatsWindow(this);

    p = Parameters::getInstance();
    p->setStats(statsWindow);

    p->setShowCountry(ui->showCountry->isChecked());
    p->setShowElevation(ui->showElevation->isChecked());
    p->setShowPopulation(ui->showPopulation->isChecked());
    p->setShowTemperature(ui->showTemperature->isChecked());

    map = new WorldMap();
    ui->renderingArea->setMap(map);

    worker = new MultithreadedWorker(map);
    // start worker thread
    worker->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_showCountry_toggled(bool checked)
{
    p->setShowCountry(checked);
}

void MainWindow::on_showElevation_toggled(bool checked)
{
    p->setShowElevation(checked);
}

void MainWindow::on_showPopulation_toggled(bool checked)
{
    p->setShowPopulation(checked);
}

void MainWindow::on_showTemperature_toggled(bool checked)
{
    p->setShowTemperature(checked);
}

void MainWindow::on_pushButton_clicked()
{
    statsWindow->show();
    statsWindow->updateCharts();
}
