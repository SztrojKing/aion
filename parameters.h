#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QMutex>
#include <statswindow.h>



class Parameters
{
public:
    Parameters();
    static Parameters* getInstance();

    bool getShowCountry() const;
    void setShowCountry(bool value);

    bool getShowElevation() const;
    void setShowElevation(bool value);

    bool getShowPopulation() const;
    void setShowPopulation(bool value);

    bool getAskRedraw() const;
    void setAskRedraw(bool value);

    bool getPaused() const;
    void setPaused(bool value);

    int getTicks() const;
    void incrementTicks();

    int getTicksPerSec() const;
    void setTicksPerSec(int value);

    int getTicksLimit() const;
    void setTicksLimit(int value);

    bool getShowTemperature() const;
    void setShowTemperature(bool value);

    StatsWindow *getStats() const;
    void setStats(StatsWindow *value);

private:

    bool showCountry;
    bool showElevation;
    bool showPopulation;
    bool showTemperature;

    bool paused;

    bool askRedraw;
    int ticks;
    int ticksPerSec;
    int ticksLimit;

    StatsWindow *stats;
};

#endif // PARAMETERS_H
