#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{

    QSurfaceFormat  format;

        format.setSamples(4);
        format.setDepthBufferSize(24);
        format.setStencilBufferSize(8);
        format.setSwapInterval(0);
        format.setSwapBehavior(QSurfaceFormat::SwapBehavior::DoubleBuffer);
        //format.setVersion(3,2);
        //format.setProfile(QSurfaceFormat::CoreProfile);
        QSurfaceFormat::setDefaultFormat(format);

    QApplication a(argc, argv);

    MainWindow w;
    w.show();
    return a.exec();
}
