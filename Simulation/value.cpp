#include "value.h"

Value::Value():value(0), maxValue(1), m_isValid(false)
{

}

bool Value::isValid() const
{
    return m_isValid;
}

void Value::setIsValid(bool isValid)
{
    m_isValid = isValid;
}

double Value::getMaxValue() const
{
    return maxValue;
}

void Value::setMaxValue(double value)
{
    maxValue = value;
}

double Value::getValue() const
{
    return value;
}

void Value::setValue(double value)
{
    this->value = value;
}
