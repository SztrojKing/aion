#ifndef WORLDMAP_H
#define WORLDMAP_H
#include <QDateTime>
#include <QVector>
#include "tile.h"

class Tile;

class WorldMap
{
public:
    WorldMap();

    QVector<QVector<Tile> > &getTiles() ;
    float *openTif(const char *path, float *noValue, int *width, int *height);
    void incrementDay();

    QDateTime getDate() const;
    void nextStep();
    void updateStats();

    void addPopulationToTotal(double n);
private:
    QVector<QVector<Tile>>  tiles;
    QVector<double> countriesId;

    QDateTime date;
    int getCountryId(double rawValue);
    double populationCount;
};

#endif // WORLDMAP_H
