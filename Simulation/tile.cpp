#include "tile.h"
#include <QtMath>
#include "worldmap.h"

Tile::Tile()
{
    temperatures.resize(12); //  allocate 12 months

}

Value& Tile::getPopulation()
{
    return population;
}

void Tile::setPopulation(const Value &value)
{
    population = value;
}

Value &Tile::getCountry()
{
    return country;
}

Value &Tile::getElevation()
{
    return elevation;
}

QVector<Value> &Tile::getTemperatures()
{
    return temperatures;
}

Value &Tile::getTemperature()
{
    return temperature;
}

void Tile::nextStep(const QDateTime &date)
{
    computeTemperature(date);
    if(population.isValid()){
        population.setValue(population.getValue()*0.99);

        map->addPopulationToTotal(population.getValue());
    }
    elevation.setValue(elevation.getValue() - 0.000027397);
}

// comput temperature using linear interpollation between current and next month  (TODO:  implement previous months)
void Tile::computeTemperature(const QDateTime &date)
{

    int currentMonth = date.date().month() ;
    int nextMonth = currentMonth < 12 ? currentMonth+1 : 1;
    if(temperatures[currentMonth-1].isValid()){
        double currentMonthValue = temperatures[currentMonth-1].getValue();
        double nextMonthValue = temperatures[nextMonth-1].getValue();
        double currentMonthRatio = (1-(double(date.date().day())/date.date().daysInMonth()));
        temperature.setValue(currentMonthValue*currentMonthRatio + nextMonthValue*(1-currentMonthRatio));
        temperature.setIsValid(true);
    }
}

void Tile::setMap(WorldMap *value)
{
    map = value;
}




