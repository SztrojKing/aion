#include "worldmap.h"
#include "gdal.h"
#include "gdal_priv.h"
#include <qdebug.h>
#include "value.h"
#include <QString>
#include <parameters.h>

WorldMap::WorldMap()
{
    date.setDate(QDate(2020, 1, 1));
    populationCount=0;
    // Initialize GDAL
    GDALAllRegister();

    // Get raster image size

    int width;
    int height;
    float popNoDataValue;
    float *populationData =  openTif("data/population/2020/data.tif", &popNoDataValue, &width, &height);

    float countryNoDataValue;
    float *countryData = openTif("data/countries/2020/data.tif", &countryNoDataValue, &width, &height);

    float elevationNoDataValue;
    float *elevationData = openTif("data/elevation/2020/data.tif", &elevationNoDataValue, &width, &height);

    float temperatureNoValue;
    // load temperatures for each months
    float *monthlyTemperatures[12];
    for(int i=0;i<12;i++){
        monthlyTemperatures[i] =  openTif(QString("data/temperature/" + QString::number(i+1).rightJustified(2, '0') + "_2019/data.tif").toUtf8(), &temperatureNoValue, &width, &height);
    }

    double maximumPopulationValue=0;
    double totalPopulation=0;

    // initialise 2D vector array
    tiles.resize(width);
    for(int i=0;i<width;i++){
        tiles[i].resize(height);
    }

    // fill tiles with data (1st pass)
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            Tile &tile = tiles[x][y];

            tile.setMap(this);

            float populationValue = populationData[x+y*width];
            float countryValue = countryData[x+y*width];
            float elevationValue = elevationData[x+y*width];

            if(populationValue!=popNoDataValue){

                totalPopulation+=populationValue;

                Value &population = tile.getPopulation();
                population.setIsValid(true);
                population.setValue(populationValue);


                if(population.getValue()>maximumPopulationValue){
                    maximumPopulationValue=population.getValue();
                }
            }

            if(qRound(countryValue)!=32767){
                Value &country = tile.getCountry();
                country.setIsValid(true);
                country.setValue(getCountryId(countryValue));
            }

            if(elevationValue!=elevationNoDataValue){
                Value &elevation = tile.getElevation();
                elevation.setIsValid(true);
                elevation.setValue(elevationValue);
            }

            for(int i=0;i<12;i++){
                float temperatureValue = monthlyTemperatures[i][x+y*width];
                if(temperatureValue!=99999){
                    Value &temperature = tile.getTemperatures()[i];
                    temperature.setIsValid(true);
                    temperature.setValue(temperatureValue);
                }
            }
        }
    }

    // set max population value for each tile
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            tiles[x][y].getPopulation().setMaxValue(maximumPopulationValue);
        }
    }
    qDebug()<<"Total population is " << totalPopulation/1000000000.0 << " billion";

    // free buffers
    CPLFree(populationData);
    CPLFree(countryData);
    CPLFree(elevationData);
    for(int i=0;i<12;i++){
        CPLFree(monthlyTemperatures[i]);
    }


    // create charts
    Parameters *p = Parameters::getInstance();
    p->getStats()->addAchart("population", new Chart("Population count", "Population (Billion)"));
    p->getStats()->addAchart("temperature", new Chart("Average temperature", "Temperature (°C)"));
}

void WorldMap::nextStep()
{
    populationCount=0;
    this->incrementDay();
    // compute tiles
    for(int x=0;x<tiles.size();x++){
        for(int y=0;y<tiles[x].size();y++){
            tiles[x][y].nextStep(date);
        }
    }

   updateStats();
}

void WorldMap::updateStats()
{
    StatsWindow *stats = Parameters::getInstance()->getStats();
    stats->getChart("population")->append(date, populationCount/1000000000.0);
   stats->getChart("temperature")->append(date, 15.2);
}

void WorldMap::addPopulationToTotal(double n)
{
    populationCount+=n;
}


QVector<QVector<Tile> >& WorldMap::getTiles()
{
    return tiles;
}

float *WorldMap::openTif(const char *path, float *noValue, int *width, int *height)
{
    // Load image
    GDALDataset* dataset = (GDALDataset*)(GDALOpen(path, GA_ReadOnly));

    // Get raster image size
    *height = dataset->GetRasterYSize();
    *width = dataset->GetRasterXSize();


    float *data = (float *) CPLMalloc((sizeof(float)) *(*width)*(*height));
    GDALRasterBand* band = dataset->GetRasterBand(1);

    *noValue = band->GetNoDataValue();

    band->RasterIO( GF_Read, 0, 0, *width, *height,
                    data, *width, *height, GDT_Float32,
                    0, 0 );

    GDALClose(dataset);
    return data;

}

void WorldMap::incrementDay()
{
    date = date.addDays(1);
}

QDateTime WorldMap::getDate() const
{
    return date;
}


int WorldMap::getCountryId(double rawValue)
{
    if(!countriesId.contains(rawValue)){
        countriesId.push_back(rawValue);
    }
    return countriesId.indexOf(rawValue);
}
