#ifndef VALUE_H
#define VALUE_H


class Value
{
public:
    Value();

    bool isValid() const;
    void setIsValid(bool isValid);

    double getMaxValue() const;
    void setMaxValue(double value);

    double getValue() const;
    void setValue(double value);

protected:
    double value;
    double maxValue;
    bool m_isValid;
};

#endif // VALUE_H
