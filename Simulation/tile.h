#ifndef TILE_H
#define TILE_H



#include <QColor>
#include <QDateTime>
#include <value.h>
class WorldMap;

class Tile
{
public:
    Tile();


    Value& getPopulation();
    void setPopulation(const Value &value);

    Value &getCountry();
    Value &getElevation();

    QVector<Value> &getTemperatures();

    Value &getTemperature();


    void nextStep(const QDateTime &date);

    void computeTemperature(const QDateTime &date);

    void setMap(WorldMap *value);

private:
    QVector<Value> temperatures;
    Value temperature;
    Value population;
    Value country;
    Value elevation;

    WorldMap *map;
};

#endif // TILE_H
