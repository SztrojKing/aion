#include "worker.h"


Worker::Worker(WorldMap *b)
    : board(b)
{
    this->setAutoDelete(true);
}

void Worker::run()
{
    board->nextStep();
}
