#include "multithreadedworker.h"
#include "parameters.h"
#include <QThread>
#include <QThreadPool>
#include <QTime>
#include <QVector>
#include "worker.h"

MultithreadedWorker::MultithreadedWorker(WorldMap *b)
    :board(b)
{
    this->chunkSize=50;
    shouldStop=false;


}

MultithreadedWorker::~MultithreadedWorker()
{

}

void MultithreadedWorker::run()
{
    threadPool.setMaxThreadCount(QThread::idealThreadCount() - 1);
   // threadPool.setMaxThreadCount(1);
    Parameters *p = Parameters::getInstance();
    QTime currentFrameDuration;

    while(!shouldStop){

            currentFrameDuration.start();

    //    lockWork();

        if(!p->getPaused()){
                threadPool.start(new Worker(this->board), QThread::Priority::HighestPriority);


        /*
                for(int i=0;i<this->board->getHeight();i+=chunkSize){
                    threadPool.start(new Worker(this->board, i, (i+chunkSize)), QThread::Priority::HighestPriority);
                }
                */
              //  board->nextStep();
                while(!threadPool.waitForDone());

                //this->board->doSingleThreadActions();



               //this->board->nextStep();
                p->incrementTicks();





            if(p->getTicksLimit()>0){
                QThread::msleep(qMax((qRound(1000.0/p->getTicksLimit()) - currentFrameDuration.elapsed()), 1));
            }
            else{
                // 1 nanosec sleep so gui thread can acquire mutex
               // QThread::usleep(1);
            }


        }
        else{
            QThread::msleep(100); // sleep to offload cpu
        }


    }
}

void MultithreadedWorker::lockWork()
{
    mutex.lock();
}

void MultithreadedWorker::releaseWork()
{
    mutex.unlock();
}

int MultithreadedWorker::getThreadCounts() const
{
    return threadPool.maxThreadCount();
}

int MultithreadedWorker::getChunkSize() const
{
    return chunkSize;
}

void MultithreadedWorker::setChunkSize(int value)
{
    chunkSize = value;
}

void MultithreadedWorker::stop()
{
    shouldStop=true;
}



