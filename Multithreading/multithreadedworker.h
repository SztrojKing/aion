#ifndef MULTITHREADEDWORKER_H
#define MULTITHREADEDWORKER_H

#include <QRunnable>
#include <QMutex>
#include <QThread>
#include <QThreadPool>
#include <QEvent>
#include <worldmap.h>

class MultithreadedWorker : public QThread
{
public:
    MultithreadedWorker(WorldMap *b);
    ~MultithreadedWorker();

    void run();

    void lockWork();
    void releaseWork();

    int getTicks() const;
    int getThreadCounts() const;

    int getChunkSize() const;
    void setChunkSize(int value);

    void stop();

private:
    WorldMap *board;
    QMutex mutex;
    int chunkSize;
    QThreadPool threadPool;
    bool shouldStop;

};

#endif // MULTITHREADEDWORKER_H
