#ifndef WORKER_H
#define WORKER_H

#include <QRunnable>
#include <worldmap.h>

class Worker : public QRunnable
{
public:
    Worker(WorldMap *b);

    // QRunnable interface
public:
    void run();

private:
    WorldMap *board;


};

#endif // WORKER_H
